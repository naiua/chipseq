#!/bin/bash

xhost +local:
docker run -it --rm \
       -v $(pwd):/workdir \
       -v /tmp/.X11-unix:/tmp/.X11-unix \
       -e DISPLAY=$DISPLAY \
       -e HOST_UID=$(id -u $USER) \
       -e HOST_GID=$(id -g $USER) \
       chipseq $@

xhost -local:
