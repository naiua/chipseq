# less
export LESSKEY=${XDG_CONFIG_HOME}/less/lesskey
export LESSHISTFILE=${XDG_CONFIG_HOME}/less/lesshst
export LESS="-i -R -M -F"

# color for ls
export LS_COLORS='di=34:ln=32:so=32:pi=33:ex=31:bd=35:cd=35:su=36:sg=36:tw=34:ow=34'

# readline init file
export INPUTRC=${XDG_CONFIG_HOME}/readline/inputrc

# R config directory
export R_PROFILE_USER=${XDG_CONFIG_HOME}/R/Rprofile
export R_LIBS_USER=${XDG_DATA_HOME}/R/library
export R_HISTFILE=${XDG_DATA_HOME}/R/Rhistory
