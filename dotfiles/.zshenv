# set XDG base directory
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_CACHE_HOME="$HOME"/.cache
export XDG_DATA_HOME="$LOCALDIR"/share

# set zsh home directory
export ZDOTDIR="$HOME"/.zsh

# pager
export PAGER="less"
export MANPAGER="less"

source "$HOME"/.profile
