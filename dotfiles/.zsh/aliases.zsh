# alias
alias ls='ls --color'
alias tree="tree -C"
alias emacs="make -C ${XDG_CONFIG_HOME}/emacs && emacs"
alias emacsd="make -C ${XDG_CONFIG_HOME}/emacs && emacs --daemon"
alias ed="make -C ${XDG_CONFIG_HOME}/emacs && emacs --daemon"
alias e="emacsclient -c"
alias emacsclientnw='TERM=screen-256color emacsclient -nw'
alias -g L="| $PAGER"
alias -g G="| grep -E"
alias -g C="| xclip -selection clipboard -i"
alias -s {pdf,PDF,djvu}=zathura
alias -s {mp4,wmv,mkv,webm,mov}=mpv
alias -s py=python
alias -s jl=julia
alias bash="bash --rcfile $HOME/.config/bash/.bashrc"
alias sqlite3="sqlite3 -init ~/.config/sqlite3/sqliterc"
alias g="git"
alias info="info --init-file ~/.config/info/.infokey"
alias python_math="python -i $HOME/bin/python/my_math.py"
alias pacc="pacman --color=always"
alias z="zathura"
alias gbook="g add etc/bookmarks/bookmarks.toml && g ci -m 'Modify bookmarks'"
alias gfile='g add Documents/memo/filelist.txt & g ci -m "Modify filelist"'
alias pymol='pymol ~/.config/pymol/.pymolrc.pml'
alias R="R --no-save --no-restore-data"
alias tdl="trash-rm"
alias t="tmux"
alias rm="trash-put"
alias em="trash-empty"
alias dl="trash-rm"
alias lst="trash-list"
alias ga="git annex"
alias se="sudoedit"
alias sctl="systemctl"
alias jctl="journalctl"
alias sudo='nocorrect sudo '
alias mg="myGitClone.sh"
alias chip="nix develop "/home/chs/.local/repo/naiua/ChIPSeq" -c "
# alias pacl='() { pacman --color=always -Ql $1 | $PAGER ;}'
# alias m='() { echo arg is $1 ;}'
# alias gsave="stash_temp=$(git stash create) && git stash store $stash_temp"
