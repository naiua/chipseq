## .zshrc --- zsh configuration file


## boot time profiling
# zmodload zsh/zprof && zprof

## keybinding
bindkey -e

## shell options
setopt nobeep
setopt extended_glob
setopt auto_cd
setopt auto_pushd
setopt pushd_ignore_dups

# treat / and = as a word separator
WORDCHARS="*?_-.[]~&;!#$%^(){}<>"

# disable C-s
stty stop undef

## functions and widgets
fpath=($ZDOTDIR/functions (N-/) $ZDOTDIR/widgets(N-/) $fpath)
autoload -Uz my-function-init
my-function-init

sticky-shift
bindkey '^m' my-enter

# clipboard interaction
bindkey '^y' my-clipboard-paste
bindkey '\ew' my-clipboard-yank

# automatically rehash
zstyle ':completion:*' rehash true

## completion
fpath=($ZDOTDIR/completions(N-/) $fpath)
autoload -Uz compinit
compinit -C

### lower case letters match their upper case counterparts
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ''
setopt complete_in_word

zmodload zsh/complist
# zstyle ':completion:*:default' list-prompt '%S%M matches%s'
bindkey -M menuselect '^n' down-line-or-history
bindkey -M menuselect '^p' up-line-or-history
zstyle ':completion:*:default' menu 'select=2'

### color setting
export LS_COLORS='di=34:ln=32:so=32:pi=33:ex=31:bd=35:cd=35:su=36:sg=36:tw=34:ow=34'
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}


## prompt
autoload -Uz promptinit vcs_info
promptinit
precmd() { print "" }

promptFunc() {
    if [ -n "$IN_NIX_SHELL" ]; then
        echo "[%F{blue}%~%f]
[nix-shell]$ "
    else
        echo "[%F{blue}%~%f] %B%(?..%?)%b
%n%F{cyan}@%f%U%m%u%F{red}❯%f "
    fi
}

PROMPT="`promptFunc`"

# PROMPT="[%F{blue}%~%f]   %n%F{cyan}@%f%U%m%u %B%(?..%?)%b
# %F{red}❯%f "

# prompt pure

## history
HISTFILE=.zsh/history.zsh
HISTSIZE=100000000
SAVEHIST=100000000
setopt hist_find_no_dups
setopt inc_append_history
setopt share_history
setopt hist_ignore_space
disable r
# add C-n, C-p search
bindkey  '^p'    history-beginning-search-backward
bindkey  '^n'    history-beginning-search-forward
bindkey  '^[[A'  history-beginning-search-backward
bindkey  '^[[B'  history-beginning-search-forward

## alias
source $ZDOTDIR/aliases.zsh

## directory hash
setopt CDABLE_VARS

## use help functionality
autoload -Uz run-help
autoload run-help-git
autoload run-help-svn
autoload run-help-svk
if (type run-help >/dev/null 2>&1); then
    unalias run-help
    alias help="run-help"
fi

## anyframe
fpath=($ZDOTDIR/anyframe(N-/) $fpath)
autoload -Uz anyframe-init
anyframe-init
zstyle ":anyframe:selector:" use fzf

# export FZF_DEFAULT_OPTS="--reverse
#                          --color=hl:1,prompt:33,spinner:136
#                          --prompt='QUERY >'
#                          --bind=ctrl-j:down,ctrl-u:page-up,ctrl-d:page-down
#                          -e
#                          --ansi"


# bindkey '^R' anyframe-widget-put-history
bindkey '^R' fzf-history-select
bindkey '^T' fzf-change-directory
bindkey '^[x' fzf-select-widget



if (which zprof >/dev/null) ;then
  zprof | less
fi

## syntax highlighting
# source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

## nix shell
if [ -d ${ZDOTDIR}/zsh-nix-shell ]; then
    source $ZDOTDIR/zsh-nix-shell/nix-shell.plugin.zsh
fi

## command not found
if [[ -f /usr/share/doc/pkgfile/command-not-found.zsh ]]; then
    source /usr/share/doc/pkgfile/command-not-found.zsh
fi

## execute fortune
if (which fortune 1>/dev/null 2>&1 && which myBoxesStone.sh >/dev/null 2>1&); then
	fortune | myBoxesStone.sh
fi

## .zshrc ends here
