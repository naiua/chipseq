FROM nixos/nix:latest

RUN nix-channel --update \
    && nix-env -iA nixpkgs.git \
    nixpkgs.perl \
    nixpkgs.MACS2 \
    nixpkgs.hisat2 \
    nixpkgs.bowtie2 \
    nixpkgs.samtools \
    nixpkgs.bedtools \
    nixpkgs.deeptools \
    nixpkgs.picard-tools \
    nixpkgs.igv \
    nixpkgs.fastp \
    nixpkgs.su-exec \
    nixpkgs.shadow \
    nixpkgs.zsh \
    && mv $(readlink /etc/passwd) \
          $(readlink /etc/group) \
          $(readlink /etc/shadow) /etc/ \
    && useradd -mU -s /root/.nix-profile/bin/zsh user \
    && echo "experimental-features = nix-command flakes" >> /etc/nix/nix.conf

# RUN echo "https://nixos.org/channels/nixos-21.11 nixos2111" >>/root/.nix-channels \
#     && nix-channel --update \
#     && nix-env -iA nixos2111.git \
#     nixos2111.perl \
#     nixos2111.MACS2 \
#     nixos2111.hisat2 \
#     nixos2111.bowtie2 \
#     nixos2111.samtools \
#     nixos2111.bedtools \
#     nixos2111.deeptools \
#     nixos2111.picard-tools \
#     nixos2111.igv \
#     nixos2111.su-exec \
#     nixos2111.shadow \
#     nixos2111.zsh \
#     && mv $(readlink /etc/passwd) \
#           $(readlink /etc/group) \
#           $(readlink /etc/shadow) /etc/ \
#     && useradd -mU -s /root/.nix-profile/bin/zsh user \
#     && echo "experimental-features = nix-command flakes" >> /etc/nix/nix.conf

COPY --chmod=744 entrypoint.sh /bin/
COPY dotfiles/ /home/user/

COPY overlays/ /overlays/
ENV NIX_PATH="$NIX_PATH:nixpkgs-overlays=/overlays"
RUN nix-env -i subread weblogo2 homer sra-tools

# RUN echo user:x:1000:1000::/home/user:/bin/sh >>/etc/passwd \
#     && echo user:x:1000: >>/etc/group \
#     && mkdir -p /home/user \
#     && chown 1000:1000 /home/user

WORKDIR ./workdir

ENTRYPOINT ["/bin/entrypoint.sh"]
