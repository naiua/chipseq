{
  description = "ChIP-Seq";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages = {
          subread = pkgs.stdenv.mkDerivation rec {
            pname = "subread";
            version = "2.0.3";
            src = pkgs.fetchurl {
              url = "https://sourceforge.net/projects/${pname}/files/${pname}-${version}/${pname}-${version}-source.tar.gz";
              sha256 = "56cef3a2f914d432713069d5c282f48831c3a1ecc89432ad5580caa322a5f56b";
            };

            buildInputs = [ pkgs.zlib.dev ];

            buildPhase =
              ''
                cd src
                make -f Makefile.Linux
              '';

            installPhase =
              ''
              shopt -s extglob
              install -Dm755 -t $out/bin ../bin/!(utilities)
            '';

            meta = with pkgs.lib; {
              description = "high-performance read alignment, quantification and mutation discovery";
              homepage = "http://subread.sourceforge.net/";
              license = licenses.gpl3;
              maintainers = [ "naiua" ];
              platforms = platforms.all;
            };
          };

          weblogo2 = pkgs.stdenv.mkDerivation rec{
            pname = "weblogo2";
            version = "2.8.2";
            src = pkgs.fetchurl {
              url = "http://weblogo.berkeley.edu//release/weblogo.${version}.tar.gz";
              sha256 = "2d3e0040c0c1e363c1dfd57f8b585387eb682ed08b2cc2fe2e4cc2a33ac52266";
            };
            buildInputs = [ pkgs.ghostscript pkgs.perl ];

            buildPhase =
              ''
                sed -i 's,$Bin,$RealBin,g' seqlogo
              '';

            installPhase =
              ''
              mkdir -p $out/{bin,opt/weblogo}
              cp -a * $out/opt/weblogo
              ln -s $out/opt/weblogo/seqlogo $out/bin/.
            '';

            meta = with pkgs.lib; {
              description = "A web based application designed to make the generation of sequence logos as easy and painless as possible";
              homepage = "http://weblogo.berkeley.edu//";
              license = licenses.mit;
              maintainers = [ "naiua" ];
              platforms = platforms.all;
            };
          };

          homer = pkgs.stdenv.mkDerivation rec {
            pname = "homer";
            version = "4.11";
            src = pkgs.fetchurl {
              url = "http://homer.ucsd.edu/${pname}/data/software/${pname}.v${version}.zip";
              sha256 = "7a86da101930b2bce1a69855a1a8181c6c886cebf7f5cc09dc7e5a7b957bac42";
            };

            buildInputs = [ pkgs.perl pkgs.unzip self.packages.${system}.weblogo2 ];

            unpackPhase =
              ''
                unzip -d homer $src
                cd homer
              '';

            buildPhase =
              ''
                cd cpp
                make clean
                make
                make clean
                cd ..
              '';

            installPhase =
              ''
              mkdir -p $out/{bin,opt/homer}
              cp -a bin/* $out/bin/
              cp configureHomer.pl $out/bin/
              shopt -s extglob
              cp -a !(bin,homer.v4.11.zip) $out/opt/homer/
            '';

            meta = with pkgs.lib; {
              description = "Software for motif discovery and next generation sequencing analysis";
              homepage = "http://homer.ucsd.edu/homer/download.html";
              license = licenses.gpl3;
              maintainers = [ "naiua" ];
              platforms = platforms.all;
            };
          };

        sra-tools = pkgs.stdenv.mkDerivation rec {
            pname = "sra-tools";
            version = "2.11.3";
            src = pkgs.fetchurl {
              url = "https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/${version}/sratoolkit.${version}-ubuntu64.tar.gz";
              sha256 = "7de3410494b0427610150d930085adf118cfac7428322c211db9d3cb08a32095";
            };

            buildInputs = [ pkgs.perl ];

            installPhase =
              ''
              install -d $out/bin
              install -d $out/usr/share/$name
              install -d $out/usr/share/$name/example/perl
              install -d $out/usr/share/$name/doc
              shopt -s extglob
              cp -ar bin/* $out/bin/
              cp -ar schema $out/usr/share/$name
              install -Dm755 -t $out/usr/share/$name/example/perl example/perl/*
              install -Dm644 -t $out/usr/share/$name/doc README*
            '';

            meta = with pkgs.lib; {
              description = "The SRA Toolkit and SDK from NCBI is a collection of tools and libraries for using data in the INSDC Sequence Read Archives.";
              homepage = "https://www.ncbi.nlm.nih.gov/sra";
              license = licenses.free;
              maintainers = [ "naiua" ];
              platforms = platforms.all;
            };
          };
        };


        devShell = pkgs.mkShell { buildInputs = [ pkgs.MACS2
                                                  pkgs.fastp
                                                  pkgs.hisat2
                                                  pkgs.bowtie2
                                                  pkgs.samtools
                                                  pkgs.bedtools
                                                  pkgs.deeptools
                                                  pkgs.picard-tools
                                                  self.packages.${system}.subread
                                                  self.packages.${system}.homer
                                                  self.packages.${system}.sra-tools
                                                ]; };
      });
}
