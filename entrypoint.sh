#!/bin/sh

USER_ID=${HOST_UID:-9001}
GROUP_ID=${HOST_GID:-9001}

echo "Starting with UID : $USER_ID, GID: $GROUP_ID"
usermod -u $USER_ID -o user
groupmod -g $GROUP_ID user

exec su-exec user "$@"
