self: super: {
  weblogo2 = with super; stdenv.mkDerivation rec{
    pname = "weblogo2";
    version = "2.8.2";
    src = pkgs.fetchurl {
      url = "http://weblogo.berkeley.edu//release/weblogo.${version}.tar.gz";
      sha256 = "2d3e0040c0c1e363c1dfd57f8b585387eb682ed08b2cc2fe2e4cc2a33ac52266";
    };
    buildInputs = [ pkgs.ghostscript pkgs.perl ];

    buildPhase =
      ''
        sed -i 's,$Bin,$RealBin,g' seqlogo
      '';

    installPhase =
      ''
        mkdir -p $out/{bin,opt/weblogo}
        cp -a * $out/opt/weblogo
        ln -s $out/opt/weblogo/seqlogo $out/bin/.
      '';

    meta = with pkgs.lib; {
      description = "A web based application designed to make the generation of sequence logos as easy and painless as possible";
      homepage = "http://weblogo.berkeley.edu//";
      license = licenses.mit;
      maintainers = [ "naiua" ];
      platforms = platforms.all;
    };
  };
}
