self: super: {
  subread = with super; stdenv.mkDerivation rec {
    pname = "subread";
    version = "2.0.3";
    src = pkgs.fetchurl {
      url = "https://sourceforge.net/projects/${pname}/files/${pname}-${version}/${pname}-${version}-source.tar.gz";
      sha256 = "56cef3a2f914d432713069d5c282f48831c3a1ecc89432ad5580caa322a5f56b";
    };

    buildInputs = [ pkgs.zlib.dev ];
    buildPhase =
      ''
        cd src
        make -f Makefile.Linux
      '';
    installPhase =
      ''
        mkdir -p $out/bin
        shopt -s extglob
        install -m755 -t $out/bin ../bin/!(utilities)
      '';
    meta = with pkgs.lib; {
      description = "high-performance read alignment, quantification and mutation discovery";
      homepage = "http://subread.sourceforge.net/";
      license = licenses.gpl3;
      maintainers = [ "naiua" ];
      platforms = platforms.all;
    };
  };
}
