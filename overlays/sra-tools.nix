self: super: {
  sra-tools = with super; stdenv.mkDerivation rec {
    pname = "sra-tools";
    version = "2.11.3";
    src = pkgs.fetchurl {
      url = "https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/${version}/sratoolkit.${version}-ubuntu64.tar.gz";
      sha256 = "7de3410494b0427610150d930085adf118cfac7428322c211db9d3cb08a32095";
    };

    buildInputs = [ pkgs.perl ];

    installPhase =
      ''
              install -d $out/bin
              install -d $out/usr/share/$name
              install -d $out/usr/share/$name/example/perl
              install -d $out/usr/share/$name/doc
              shopt -s extglob
              cp -ar bin/* $out/bin/
              cp -ar schema $out/usr/share/$name
              install -Dm755 -t $out/usr/share/$name/example/perl example/perl/*
              install -Dm644 -t $out/usr/share/$name/doc README*
            '';

    meta = with pkgs.lib; {
      description = "The SRA Toolkit and SDK from NCBI is a collection of tools and libraries for using data in the INSDC Sequence Read Archives.";
      homepage = "https://www.ncbi.nlm.nih.gov/sra";
      license = licenses.free;
      maintainers = [ "naiua" ];
      platforms = platforms.all;
    };
  };
}
