self: super: {
  homer = with super; stdenv.mkDerivation rec {
    pname = "homer";
    version = "4.11";
    src = pkgs.fetchurl {
      url = "http://homer.ucsd.edu/${pname}/data/software/${pname}.v${version}.zip";
      sha256 = "7a86da101930b2bce1a69855a1a8181c6c886cebf7f5cc09dc7e5a7b957bac42";
    };

    buildInputs = [ pkgs.unzip pkgs.weblogo2 ];

    unpackPhase =
      ''
        unzip -d homer $src
        cd homer
      '';

    buildPhase =
      ''
        cd cpp
        make clean
        make
        make clean
        cd ..
      '';

    installPhase =
      ''
        mkdir -p $out/{bin,opt/homer}
        cp -a bin/* $out/bin/
        cp configureHomer.pl $out/bin/
        shopt -s extglob
        cp -a !(bin,homer.v4.11.zip) $out/opt/homer/
      '';

    meta = with pkgs.lib; {
      description = "Software for motif discovery and next generation sequencing analysis";
      homepage = "http://homer.ucsd.edu/homer/download.html";
      license = licenses.gpl3;
      maintainers = [ "naiua" ];
      platforms = platforms.all;
    };
  };
}
